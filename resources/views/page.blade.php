
@extends('layouts.app')

@section('content')
  {{-- @while(have_posts()) @php the_post() @endphp
    @include('partials.page-header')
    {{-- Include right page here ? --}}
    
  
  {{-- @endwhile --}} 

  {{-- @include('page-about')
  @include('page-bikes') --}}

  @include('partials.content-page-'.get_post_field( 'post_name' ))

@endsection
