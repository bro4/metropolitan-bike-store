{{-- Bikes --}}
@php
$zubehoer_types = get_terms( 'zubehoer_type' );
// var_dump($zubehoer_types);
@endphp

<div class="container d-flex flex-column">
    @foreach($zubehoer_types as $index => $el)
    <div id="{{$el->slug}}">
        @php
            $args = array(
                'post_type' => 'zubehoer',
                'posts_per_page' => -1,
                'order' => 'DESC',
                'tax_query' => array(
                    array(
                        'taxonomy' => 'zubehoer_type',
                        'field'    => 'slug',
                        'terms' => array ( $el->name )
                )
            )
            );
            $categories = get_posts($args);
            // var_dump($categories);
        @endphp
        <div class="bikes-box mb-10 mb-lg-20">
            <h2> {{$el->name}}</h2>
            <div class="swiper-container pb-9 pb-lg-10">
                <!-- Additional required wrapper -->
                <div class="swiper-wrapper">
                    @foreach ($categories as $category)
                    <!-- Slides -->    
                    <div class="bike-content swiper-slide">
                        <div class="content-left">
                            <p class="content-title">{{$category->post_title}}</p>
                            <p>{!! apply_filters('the_content', $category->post_content) !!}</p>
                            {{-- <button data-toggle="modal" data-target="#zubehormodal" class="red-btn">Übersicht</button> --}}
                            <p class="preis">Preis: {{ get_field('preis', $category->ID)}}</p>
                        </div>
                        <div class="bike-img">
                            {!!get_the_post_thumbnail($category->ID)!!}
                        </div>
                        <!-- If we need navigation buttons -->
                    </div> 
                    @endforeach
                </div>
                {{-- <div class="arrow-container"> --}}
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-pagination"></div>
                    <div class="swiper-button-next"></div>
                {{-- </div>	            			 --}}
            </div>                      
        </div>
    </div>
    @endforeach
</div>