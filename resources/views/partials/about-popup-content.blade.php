
<div class="blog">
  <div class="content-wrapper blog-post mt-10 mt-lg-30" >
    <p class="date">{{date('j.m.Y', strtotime($post->post_date))}}</p>
    <div class="content" data-postid={{$el->ID}}> <!-- data-toggle="modal" data-target="#blog-popup" -->
        <h2> {{get_the_title()}}</h2>

        @if (get_the_post_thumbnail($post->ID))
        <div class="content-img">
        <img src ="{!! get_the_post_thumbnail_url($post->ID)!!}"> 
        </div>
      @else 
      @endif


      {{-- <div class="content-img">
       <img src ="{!! get_the_post_thumbnail_url($post->ID)!!}" > 
      </div> --}}
      <p class="mt-1 mt-lg-6">{!! apply_filters('the_content',get_the_content()) !!}</p>
    </div>
  </div>
</div>