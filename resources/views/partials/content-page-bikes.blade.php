@php
$zubehoerSatz = get_field('zubehoreinfuhrungssatz');
$biketypes = get_terms( 'bike_type' );
// var_dump($biketypes);
@endphp

<section id="bikes">
	<h1 class="page-title">{{get_the_title()}}</h1>
	<div class="title-img">
		<img src="{{get_the_post_thumbnail_url()}}">
	</div>
	<div class="container d-flex flex-column">
		<h3 class="introduction">
			{{-- {!!get_the_content()!!} --}}
			{!! apply_filters('the_content',get_the_content()) !!}<br>
		</h3>
		<div class="content-wrapper flex-column">

			{{-- Bikes categories --}}
			<div class="categories-btn text-wrap justify-content-center justify-content-lg-between">
				@foreach($biketypes as $index => $biketype)
				<a class="categories" href="#{{$biketype->slug}}">
					<button class="red-btn-categories d-flex flex-row justify-content-between align-items-center">
						<div></div>
						<h2>{{$biketype->name}}</h2>
						<img class="align-self-center" src="@asset('images/arrow-down.svg')">
					</button>
				</a>
				@endforeach
				<a class="categories" href="#zubehoer">
					<button class="red-btn-categories d-flex flex-row justify-content-between align-items-center">
						<div></div>
						<h2>Zubehör</h2>
						<img class="align-self-center" src="@asset('images/arrow-down.svg')">
					</button>
				</a>
			</div>
			<div class="sales-btn-box categories-btn align-self-center">
				<button class="sales-btn"><a href="{{get_the_permalink(25)}}">
						<h2>SALE</h2>
					</a></button>
			</div>
		</div>

		{{-- Zubehör --}}
		{{--<div class="bikes-box mb-30">
			<h2>Zubehör</h2>
			<div class="bike-content">
				<div class="content-left">
					<p class="pb-4">{{$zubehoerSatz}}</p>
					<button data-toggle="modal" data-target="#zubehormodal" class="red-btn">Übersicht</button>
				</div>
				<div class="bike-img">
					<img src="{{get_field('zubehorbild')}}" alt="">
				</div>
			</div>
		</div>--}}

		{{-- Zubehörmodal --}}
		{{-- @include('partials.sales-popup') --}}


		{{-- Bikes --}}
		@foreach($biketypes as $index => $biketype)
		<div id="{{$biketype->slug}}">
			@php
			$args = array(
			'post_type' => 'bikes',
			'posts_per_page' => -1,
			'order' => 'DESC',
			'tax_query' => array(
			array(
			'taxonomy' => 'bike_type',
			'field' => 'slug',
			'terms' => array ( $biketype->name )
			)
			)
			);
			$categories = get_posts($args);
			// var_dump($categories);
			@endphp
			<div class="bikes-box">
				<h2> {{$biketype->name}}</h2>
				<div class="swiper-container pb-9 pb-lg-10">
					<!-- Additional required wrapper -->
					<div class="swiper-wrapper">
						@foreach ($categories as $category)
						<!-- Slides -->
						<div class="bike-content swiper-slide">
							<div class="content-left">
								<p class="content-title">{!! str_replace('<br> ', '<br />', $category->post_title) !!}
								</p>
								<p>{!! apply_filters('the_content', $category->post_content) !!}</p>
								{{-- <button data-toggle="modal" data-target="#zubehormodal"
									class="red-btn">Übersicht</button> --}}
								@if(get_field('preis', $category->ID)!="")
								<p class="preis">Preis: {{ get_field('preis', $category->ID)}}</p>
								@endif
							</div>
							<div class="bike-img">
								{!!get_the_post_thumbnail($category->ID)!!}
							</div>
							<!-- If we need navigation buttons -->
						</div>
						@endforeach
					</div>
					{{-- <div class="arrow-container"> --}}
						<div class="swiper-button-prev"></div>
						<div class="swiper-pagination"></div>
						<div class="swiper-button-next"></div>
						{{--
					</div> --}}

				</div>
			</div>
		</div>
		@endforeach

		<a class="mb-10 mb-lg-30 align-self-center" href={{get_field('cannondales_webseite')}} target="_blank">
			<button class="cannondale-btn">Zum aktuellen Sortiment auf <span>www.cannondale.com</span></button>
		</a>
	</div>
</section>

<div id="zubehoer" class="page-title mb-6 mb-lg-20">
	<h1 class="container">Zubehör</h1>
</div>


@include('partials.zubehoer')