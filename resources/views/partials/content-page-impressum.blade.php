@php
 $impressum = get_post(197);   
@endphp

<section id="impressum">
	<h1 class="page-title">{{get_the_title()}}</h1>
	<div class="container d-flex flex-column mt-5 mt-lg-10">
        <p>{!! apply_filters('the_content',get_the_content()) !!}</p>
    </div>
</section>
