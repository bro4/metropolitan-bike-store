@php
    $sale = get_post(25);
    // var_dump($sale);
    $args = array(
                        'post_type' => 'bikes',
                        'posts_per_page' => -1,
                        'order' => 'DESC',
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'sale',
                                'field'    => 'slug',
                                'terms' => array ( 'sales' )
                            )
                    )
                    );
  
    $posts = get_posts($args); 
    // var_dump($posts);
    $preis = get_field('preis');
    // var_dump($posts)
@endphp

<section id='sales'>
    <div class="container mb-10 ">
        {{-- <a class="sales-page-btn" href="{{get_the_permalink(6)}}">Zurück zu den Bikes</a> --}}

        <button class="sales-page-btn d-flex flex-row justify-content-center align-items-center">
            <img class="mobile" src="@asset('images/arrow-back-sm.svg')">
            <img class="desktop" src="@asset('images/arrow-back.svg')">
            <a class="pl-3 pl-lg-5" href="{{get_the_permalink(6)}}">Zurück zu allen Bikes</a>
        </button>

        <div class="content-wrapper mt-12 mt-lg-20 mb-10 mb-lg-16">
            <div class="d-flex flex-column flex-lg-row justify-content-lg-between">
                <h2>SALE</h2>
                <h2 class="content">{!! apply_filters('the_content',get_the_content()) !!}</h2>
            </div>
        </div>
    
        @foreach($posts as $index => $el)
            <div class="bikes-box mb-10 mb-lg-20">
                {{-- <h2>Zubehör</h2> --}}
                <div class="bike-content">
                    <div class="content-left">
                        <p class="content-title">{!!$el->post_title!!}</p>
                        <p>{!! apply_filters('the_content', $el->post_content) !!}</p>
                        {{-- <button data-toggle="modal" data-target="#zubehormodal" class="red-btn">Übersicht</button> --}}
                        <p class="preis">Preis: {{get_field('preis', $el->ID)}}</p>
                    </div>
                    <div class="bike-img">
                        {{-- <img src="{{$el->post_thumbail}}" alt=""> --}}
                        {!!get_the_post_thumbnail($el->ID)!!}
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</section>  

@include('partials.sales-popup')


