@php
$about = get_post(16);
$about_ID = $about->ID;
$bikes = get_post(6);
$bikes_ID = $bikes->ID;
$service = get_post(18);
$service_ID = $service->ID;
$blog = get_post(27);
$blog_ID = $blog->ID;
$footer = get_post(3);
// var_dump($about);
// var_dump($about_ID);
// {!!$footer->post_content!!}
@endphp


<header>
    <nav class="container navbar navbar-expand-lg">
      <a class="navbar-brand" href="{{get_the_permalink($about_ID)}}">
        <img src="@asset('images/logo.svg')">
      </a>

      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        {{-- <span class="navbar-toggler-icon"></span> --}}
        {{-- <div class="navbar-toggler-icon"></div> --}}
        <div id="nav-icon3" class="navbar-toggler-icon">
          <span></span>
          <span></span>
          <span></span>
          <span></span>
        </div>
      </button>
   
      <div class="collapse navbar-collapse justify-content-start justify-content-lg-end" id="navbarNav">
        <ul class="navbar-nav align-items-center my-5 my-sm-0">
          <li class="nav-item ">
            <a class="nav-link " href="{{get_the_permalink($about_ID)}}">{{$about->post_title}}</a>
          </li>
          <li class="nav-item">
            <a class="nav-link"href="{{get_the_permalink($bikes_ID)}}">{{$bikes->post_title}}</a>       
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{get_the_permalink($service_ID)}}">{{$service->post_title}}</a>               
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{get_the_permalink($blog_ID)}}">{{$blog->post_title}}</a>               
          </li>
          <li id="nav-address" class="address">
            <p> {!! apply_filters('the_content', $footer->post_content) !!} </p>
          </li>
        </ul>
      </div>
    </nav>
</header>



