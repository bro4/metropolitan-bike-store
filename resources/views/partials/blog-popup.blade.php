
<div id="blog-popup" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-full" role="document">
    <div class="modal-content">
      <div class="close mt-5 mr-3 align-self-end" data-dismiss="modal" aria-label="Close">
      </div>
      <div class="container d-flex flex-column">
        <div class="modal-data">
            {{-- data from ajax call --}}
        </div>
        <div class="toTop align-self-end">
          <img class="toTopImg" src="@asset('images/arrow-to-top.svg')">
        </div>        
      </div>
    </div>
  </div>
</div>