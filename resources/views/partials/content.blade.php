<article @php post_class() @endphp>
  <header>
    <h2 class="entry-title"><a href="{{ get_permalink() }}">{!! get_the_title() !!}</a></h2>
    This is content.blade.php <br>
    {{-- @include('partials/about') --}}
  </header>
  <div class="entry-summary">
    @php the_excerpt() @endphp
  </div>
</article>
