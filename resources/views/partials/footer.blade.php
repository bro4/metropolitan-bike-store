
@php 

$mypost_id = get_queried_object()->ID;
// the_field( "footersatz", $mypost_id );
$footer = get_post(3);
// var_dump($footer);
// var_dump($footer->ID);
@endphp

<footer>
  <div class="container mt-lg-10">

    {{-- First part --}}
    <div class="d-flex flex-column flex-lg-row justify-content-between mb-15 mb-lg-20">
      <img src="@asset('images/rad.jpg')" alt="" id="rad" class="order-2 order-lg-1 align-self-center mt-8 mt-lg-0">
      <h2 class="footer-satz order-1 order-lg-2">
        {!!get_field('footersatz')!!}
        {{-- {!! apply_filters('the_content',get_the_content()) !!} --}}
      </h2>
    </div>

    {{-- second part --}}
    <div class="address footer-bottom d-flex flex-column flex-lg-row justify-content-between">
      {{-- left --}}
      <div>      
        <p> {!! apply_filters('the_content', $footer->post_content) !!} </p>

      </div>

      {{-- right --}}
      <div class="d-flex flex-column justify-content-between align-items-lg-end text-right">

        {{-- top --}}
        <div class="toTop align-self-end">
          <img class="toTopImg" src="@asset('images/arrow-to-top.svg')">
        </div>

        {{-- bottom --}}
        <div class="impressum d-flex flex-row justify-content-between justify-content-lg-end align-items-right ">
          <a href={{get_the_permalink(197)}}><p class="mr-3 mr-md-6">Impressum</p></a>
          <a href={{get_the_permalink(199)}}><p>Datenschutz</p></a>
        </div>
      </div>
    </div>

  </div>
</footer>
