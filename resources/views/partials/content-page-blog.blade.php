@php 
  $args = array(
    'post_type' => 'post',
    'posts_per_page' => -1,
    'order' => 'DESC',
    'orderby' => 'date',
  );
  
    // global $post;
    $posts = get_posts($args); 
    // var_dump($posts)

    @endphp

<section id="blog"> 
  <h1 class="page-title">{{get_the_title()}}</h1>
  <div class="container">
    <h3 class="introduction">
      {!! apply_filters('the_content',get_the_content()) !!}
    </h3>

    @foreach($posts as $index => $el)
    {{-- @php
         $content = $el->post_content;  
        //  var_dump($content);
         $button = `<button data-postid={{$el->ID}} class="red-btn">Alles lesen</button>`
    @endphp --}}
    <div class="content-wrapper blog-post" >
      <p class="date">{{date('j.m.Y', strtotime($el->post_date))}}</p>
      <div class="content d-flex flex-column" data-postid={{$el->ID}}> <!-- data-toggle="modal" data-target="#blog-popup" -->
        <h2> {!!$el->post_title!!}</h2>
        @if (get_the_post_thumbnail($el->ID))
          <div class="content-img">
          <img src ="{!! get_the_post_thumbnail_url($el->ID)!!}"> 
          </div>
        @else 
        @endif
        <p class="blog-p">{!! get_field('beitragteaser', $el->ID) !!}</p>
        @if ($el->post_content)
         <button data-postid={{$el->ID}} class="red-btn align-self-center"><p>Alles lesen</p></button>
        @else
        @endif
      </div>
    </div>
    @endforeach
  </div>
</section>

@include('partials.blog-popup')

