@php

$zubehoerSatz = get_field('zubehoreinfuhrungssatz');

        $args = array(
      'post_type' => 'zubehoer',
      'posts_per_page' => -1,
      'order' => 'DESC',
      // 'orderby' => 'date',
    );
    
      $zubehoer = get_posts($args); 
      // var_dump($zubehoer);
@endphp
    

    
{{--Zubehörmodal  --}}
<div class="modal" tabindex="-1" role="dialog" id="zubehormodal">
  <div class="modal-dialog modal-full" role="document">
    <div class="modal-content">
      <div class="container d-flex flex-column">
          {{-- <button type="button" class="m-5" data-dismiss="modal" aria-label="Close"> --}}
          {{-- <span aria-hidden="true"></span> --}}
          <div class="close mt-5 align-self-end" data-dismiss="modal" aria-label="Close">
          </div>
          {{-- </button> --}}
          <div class="content-wrapper mt-16">
            <div class="d-flex flex-column flex-lg-row justify-content-lg-between">
                <h2>Zubehör</h2>
                <p class="content zubehoer-content">{{$zubehoerSatz}}</p>
            </div>
          </div>
          <div class="zubehoer-box" >
            @foreach($zubehoer as $index => $el)
              <article class="zubehoer-beschreibung">
                <div class="zubehoer-img">                        
                  {!!get_the_post_thumbnail($el->ID)!!}                    
                </div>
                <p>{{$el->post_title}}</p> 
                <p>{{$el->post_content}}</p>
              </article>
            @endforeach
          </div>
        <div class="toTop align-self-end">
          <img class="toTopImg" src="@asset('images/arrow-to-top.svg')">
        </div>
      </div>
    </div>
  </div>
</div>