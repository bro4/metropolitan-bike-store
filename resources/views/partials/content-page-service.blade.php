@php
$allgemeiner_service = get_field('allgemeiner_service');
$benutzerdefinierte_service = get_field('benutzerdefinierte_service');
@endphp

<section id="service">
    <h1 class="page-title">{{get_the_title()}}</h1>
    <div class="title-img">
        <img src="{{get_the_post_thumbnail_url()}}">
    </div>
    <div class="container">
        <h3 class="introduction">
            {{-- {!!get_the_content()!!} --}}
            {!! apply_filters('the_content',get_the_content()) !!}
        </h3>
        <div class="content-wrapper">
            <h2>Service allgemein</h2>
            <div class="content">{!!$allgemeiner_service!!}</div>
        </div>
        <div class="content-wrapper">
            <h2>Service Cannondale, <br>Lefty und Headshok</h2>
            <div class="content">{!!$benutzerdefinierte_service!!}</div>
        </div>
    </div>
</section>