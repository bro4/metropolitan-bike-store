@php
 $datenschutz = get_post(199);   
@endphp

<section id="datenschutz">
	<h1 class="page-title">{{get_the_title()}}</h1>
	<div class="container d-flex flex-column mt-5 mt-lg-10">
        <p>{!! apply_filters('the_content',get_the_content()) !!}</p>
    </div>
</section>
