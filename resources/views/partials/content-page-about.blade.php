@php
    $about = get_post(16);
    $about_ID = $about->ID;
    // var_dump(get_field('uber_mich'));
    // var_dump($about);
    $uber_mich_bild = get_field('uber_mich_bild');
    $location = get_field('standort');

    $args = array(
                        'post_type' => 'post',
                        'posts_per_page' => 1,
                        'order' => 'DESC',
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'category',
                                'field'    => 'slug',
                                'terms' => array ( 'homepage' )
                            )
                    )
                    );
  
    $posts = get_posts($args); 
    // var_dump($posts);

@endphp


<section id='about'>
    <h1 class="page-title">{{$about->post_title}}</h1>
    <div class="title-img">
        <img src="{{get_the_post_thumbnail_url()}}">
        @foreach ($posts as $index => $post) 
        <div class="container-1">
            <div class=" info-box" data-postid={{$post->ID}}>
                <h3>{{$post->post_title}}</h3>

                @if ($post->post_content)
                <button data-postid={{$post->ID}} class="about-btn">
                    <p>Mehr dazu<p>
                </button>
                @else
                @endif
            </div>
        </div>
        @endforeach
        @include('partials.about-popup')
    </div>
    <div class="container">
        <h3 class="introduction">
            {{-- {!!get_the_content()!!} --}}
            {!! apply_filters('the_content',get_the_content()) !!}
        </h3>

        <div class="content-wrapper">
            <div>
                <h2>Über mich</h2> 
                <div class="about-img"><img src="{!!$uber_mich_bild!!}"></div>
            </div>
            <div class="content">{!!get_field('uber_mich')!!}</div>
        </div>

        <div class="content-wrapper">
            <h2>Der Laden</h2>
             <div class="content"> 
                <div class="content-img laden-img">
                    <img src="{!!get_field('laden_bild')!!}">
                </div>
                {!!get_field('der_laden')!!}
                </div>
           
        </div>

        <div class="content-wrapper">
            <h2>Standort</h2>
            <div class="content">
                <div class="acf-map" >
                    <div class="marker" data-lat="<?php echo esc_attr($location['lat']); ?>" data-lng="<?php echo esc_attr($location['lng']); ?>"></div>
                </div>
            </div>
        </div> 
    </div> 
</section>

    

