import Swiper, { Navigation, Pagination } from 'swiper';
// import Swiper from 'swiper';
// import 'swiper/swiper-bundle.css';

export default {
  init() {
    console.log('bikes init');
    let buttons = document.querySelectorAll('#bikes .categories');
    console.log(buttons);
    scrollToCategory(buttons, 107);

    function scrollToCategory(el, offsetValue) {
      $(el).click(function (event) {
        console.log($(event.target).text());
        event.preventDefault();
        var hash = this.hash;
        // console.log(hash)
        $('html, body').animate(
          {
            scrollTop: $(hash).offset().top - offsetValue,
          },
          600
        );
      });
    }
  },

  finalize() {
    Swiper.use([Navigation, Pagination]);

    // JavaScript to be fired on the home page, after the init JS
    setTimeout(() => {
      console.log('timeout');
      setupSwiper();
      // centerSwiperArrows();
    }, 500);

    // $( window ).resize(function() {
    //  centerSwiperArrows();
    // })

    function setupSwiper() {
      // console.log('swiper initialised')
      var swiper = new Swiper('.swiper-container', {
        // Default parameters
        slidesPerView: 1,
        spaceBetween: 10,
        autoHeight: true,
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },
        pagination: {
          el: '.swiper-pagination',
          clickable: true,
        },
      });

      console.log($(this).find('.swiper-button-next').get());
    }

    function centerSwiperArrows() {
      let swiperImg = $('.bike-img');
      let height = 0;

      $(swiperImg).each(function () {
        if (height < $(this).height()) {
          height = $(this).height();
          console.log(height);
        }
        $('.arrow-container').height(height);
      });
    }
  },
};
