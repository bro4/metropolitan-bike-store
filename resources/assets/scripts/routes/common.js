export default {
  init() {
    // JavaScript to be fired on all pages
  },
  finalize() {
        // JavaScript to be fired on all pages, after page specific JS is fired

    console.log('common')


    setupLoadPopupBlog()
    setupLoadPopupHomepage()

    let footerArrow = document.querySelector('footer .toTop')
    backToTop(footerArrow, 107)

    let modalArrow = document.querySelector('.modal .toTop')
    backToTopModal(modalArrow, 107)
    
    checkScrollPos()


    function backToTop (el, offsetValue) {
      $(el).click(function(event) {
          $('html, body').animate({
              scrollTop: $('body').offset().top -offsetValue,
          }, 600);
      })
    }

    function backToTopModal (el, offsetValue ) {
      $(el).click(function(event) {
        console.log('clicked')
          $('.modal').animate({
              scrollTop: $('body').offset().top -offsetValue,
          }, 600);
      })
    }

    function checkScrollPos () {
      $('.modal .toTop').css('display', 'none')
       $('.modal').scroll(function () {
        if ($(this).scrollTop()>10) {
          $('.toTop').css('display', 'flex')
        }  else {
            $('.modal .toTop').css('display', 'none')
          }
      })
    }

  //   navbarOverlay()
  //   function navbarOverlay () {
  //     $('#nav-icon3').on('click', function () {
  //     if (!$('#navbarNav').hasClass('show')) {
  //       console.log('has class show')
  //       $('nav').css('height', '100vh')
  //     } else {
  //       console.log('doesnt have classe show')
  //       $('nav').css('height', 'auto')
  //     }
  //   })
  // }


    // Pop up blog 
    function setupLoadPopupBlog() {   
    $('#blog button').on('click', function () {
        // console.log($(this).attr('data-postid'));
        // get id from clicked post
        let postId = parseInt($(this).attr('data-postid'));
        // call Ajax Call function with postID as parameter
        loadPopup(postId);
      })
     }
    
    //  AJAX Call
    function loadPopup(postId) {
      // console.log('load called')
      // Ajax call made on url, start action
     $.ajax({
       url: `//${location.host}/wp-admin/admin-ajax.php`,
       data: {
         action: 'loadBlogPopup',
    // how are we getting content here ? coming by default ? 
         postId: postId,
       },
       type: 'POST',
       dataType: 'html',
      //  after data retrieved, display them in html
       success: function (data) {
         console.log('data', data)
         $('#blog-popup .modal-data').html(data);
        //  Open modal after data loaded for nicer visual effect
         $('#blog-popup').modal('show');
       },
     });
    }

    function setupLoadPopupHomepage() {   
      $('.info-box button').on('click', function () {
          console.log($(this).attr('data-postid'));
          // get id from clicked post
          let postId = parseInt($(this).attr('data-postid'));
          // call Ajax Call function with postID as parameter
          postAjaxCall(postId);
        })
       }
      
      //  AJAX Call
      function postAjaxCall(postId) {
        // console.log('load called')
        // Ajax call made on url, start action
       $.ajax({
         url: `//${location.host}/wp-admin/admin-ajax.php`,
         data: {
           action: 'getPostData',
      // how are we getting content here ? coming by default ? 
           postId: postId,
         },
         type: 'POST',
         dataType: 'html',
        //  after data retrieved, display them in html
         success: function (data) {
          //  console.log('data', data)
           $('#about-popup .modal-data').html(data);
          //  Open modal after data loaded for nicer visual effect
           $('#about-popup').modal('show');
         },
       });
      }
  },
}