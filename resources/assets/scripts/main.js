// import external dependencies
import 'jquery';

// Import everything from autoload
import './autoload/**/*'

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import about from './routes/about';
import bikes from './routes/bikes';

/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
  common,
  // Home page
  home,
  // About Us page, note the change from about-us to aboutUs.
  about,
  bikes,
});

// Load Events
jQuery(document).ready(() => routes.loadEvents());



    // changeNavColor()
    colorActiveLink()
    // JavaScript to be fired on the home page
    changeNavColortoGrey ()




   /* eslint-disable */

// Changing background-color and navbar brand in mobile version 


function changeNavColortoGrey () {
  $('.navbar-toggler').click(function () {
    if (!$('.navbar-collapse').hasClass('show')) {
     $('header').addClass( 'grey')
     $('.navbar-brand').css('visibility', 'hidden')
    } else {
     $('header').removeClass('grey')
     $('.navbar-brand').css('visibility', 'visible')
    }
  })
}

// Active Link appears blue in navbar in mobile version

function colorActiveLink () {
     $(document).ready(function() {
      $("[href]").each(function() {
          if (this.href == window.location.href) {
              $(this).addClass("active");
              console.log(this)
          }
      });
  });
}

$(document).ready(function(){
	$('#nav-icon1,#nav-icon2,#nav-icon3,#nav-icon4').click(function(){
		$(this).toggleClass('open');
	});
});